export default {
    data() {
        return {
            form: {
                users:[],
            },
            errors: {},
            file:'',
            users: [],
            active:false,
        }
    },
    mounted () {
        this.active = true;
    },
    methods: {
        onFileChanged(event) {
            this.formData = new FormData();
            this.file = event.target.files[0];
        },
        generateForm() {
            var newData = JSON.stringify(this.form);
            const myform = new FormData();
            myform.append('form', newData);
            myform.append('image', this.file );

            return myform
        },
        getUsers() {
            this.axios.get('users/list').then( (response)=> {
              this.users = response.data;
            });
        }
    },

    watch: {
        active(val) {
            this.getUsers();
        }
    }

}
