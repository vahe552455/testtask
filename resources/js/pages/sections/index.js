require('../../bootstrap');

import Vue from 'vue';
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios);

import SectionList from './SectionList';

new Vue({
    el: '#section',
    template: '<SectionList/>',
    components: {SectionList}
});
