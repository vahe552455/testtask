@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="alert alert-secondary" role="alert">
            User
        </div>
        @if($errors->first())
            <div class="alert alert-danger" role="alert">
                {{$errors->first()}}
            </div>
        @endif
        <form method="POST" action="{{route('users.update', $user->id)}}">
            @csrf
            {{ method_field('PUT') }}
            <div class="mx-3">
                <div class="form-group">
                    <label for="formGroupExampleInput">Name</label>
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Name"
                           value="{{$user->name}}" name="name">
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput2">Email</label>
                    <input type="email" class="form-control" id="formGroupExampleInput2" placeholder="Email Address"
                           value="{{$user->email}}" name="email">
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput2">Password</label>
                    <input type="password" class="form-control" id="formGroupExampleInput2" placeholder="Password"
                           name="password">
                </div>
                <div class="form-group text-right mt-5">
                    <input type="submit" class="btn btn-primary" value="Update">
                </div>
            </div>
        </form>
    </div>

@endsection
