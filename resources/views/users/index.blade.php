@extends('layouts.app')

@section('content')
    <div class="container">
        @if(Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
        @endif
        @if($errors->first())
            <div class="alert alert-danger">
                {{ $errors->first() }}
            </div>
        @endif
        <div class="mb-3 text-right">
            <a href="{{route('users.create')}}" class="btn btn-primary">
                Add
            </a>
        </div>

        <table class="table table-hover">
            {{--        <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">Handle</th>
                    </tr>
                    </thead>--}}
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{$user->name}}</th>
                    <td> {{$user->email}}</td>
                    <td>{{$user->updated_at}}</td>
                    <td>
                        <a href="{{route('users.edit',$user->id)}}" class="btn btn-primary"> Edit</a>
                        <form action="{{route('users.destroy',$user->id)}}" method="POST" class="d-inline-block" >
                            @csrf()
                            {!! method_field('delete') !!}
                            <button type="submit" class="btn btn-secondary">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $users->links() }}

    </div>



    {{--    @dd($users);--}}
@endsection
@push('scripts')
    <script src="{{mix('js/sections.js')}}" type="application/javascript" ></script>
@endpush
