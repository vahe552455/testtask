<?php


namespace App\Repositories;

use App\Interfaces\UserInterface;
use App\User;
use Illuminate\Support\Arr;

class UserRepository extends BaseRepository implements UserInterface
{
    /**
     * UserRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * @param $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index($request)
    {
        $users = $this->model->where('id', '!=', auth()->user()->id)->paginate(10);

        return view('users.index', compact('users'));
    }

    /**
     * @param $id
     * @param $request
     * @return mixed|void
     */
    public function edit($id, $request)
    {
        // TODO: Implement edit() method.
        $user = $this->model->find($id);

        return $user;
    }

    /**
     * @param $request
     * @return mixed|void
     */
    public function store($request)
    {
        // TODO: Implement store() method.
        Arr::set($data, 'password', bcrypt($data['password']));
        $user = $this->model->create($request);

        return $user;

    }

    /**
     * @param $request
     * @return mixed
     */
    public function getList($request)
    {
      return $this->model
          ->select(['id','name'])
          ->get()
          ->toArray();
    }

    /**
     * @param $id
     * @param $data
     * @return mixed|void
     */
    public function update($id, $data)
    {
        $user = $this->model->find($id);
        Arr::set($data, 'password', bcrypt($data['password']));
        $user->update($data);

        return $user;
    }

    /**
     * @param $id
     * @return mixed|void
     * @throws \Exception
     */
    public function delete($id)
    {
       $user =  $this->model->find($id);

       return $user->delete();
    }
}
