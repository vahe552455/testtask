<?php

namespace App\Repositories;

class BaseRepository
{
    /**
     * @var
     */
    protected $model;

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $id
     * @param $data
     */
    public function update($id, $data)
    {

    }

    /**
     * @param $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($request)
    {
        return view(request()->path().'/index');
    }
}
