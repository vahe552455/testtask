<?php

namespace App\Repositories;

use App\Interfaces\SectionInterface;
use App\Models\Section;
use Illuminate\Http\File;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class SectionRepository extends BaseRepository implements SectionInterface
{
    /**
     * SectionRepository constructor.
     * @param Section $section
     */
    public function __construct(Section $section)
    {
        $this->model = $section;
    }

    /**
     * @param $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function get($request)
    {
        $section = $this->model
            ->with('users:id,name')
            ->paginate(10);

        return $section;
    }

    /**
     * @param $id
     * @param $request
     * @return mixed|void
     */
    public function edit($id, $request = null)
    {
        // TODO: Implement edit() method.
        $section = $this->model
            ->where('id', $id)
            ->with('users:id')
            ->first()
            ->toArray();

        $users = Arr::pluck($section['users'],'id');
        Arr::set($section, 'users' , $users);

        return $section;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        // TODO: Implement store() method.
        $form = $data['form'];

        if(Arr::get($data,'image' )) {
            $name = $data['image']->getClientOriginalName();
            $name = str_random(10).time().$name;
            Storage::disk('public')->putFileAs('logo' , $data['image'], $name);
            $form['logo'] = $name;
        }

        $section = $this->model->create($data);
        $section->users()->attach($form['users']);

        return $section;
    }

    /**
     * @param $id
     * @param $data
     * @return mixed|void
     */
    public function update($id, $data)
    {
        $form = $data['form'];

        if(Arr::get($data,'image' )) {
            $name = $data['image']->getClientOriginalName();
            $name = str_random(10).time().$name;
            Storage::disk('public')->putFileAs('logo' , $data['image'], $name);
            $form['logo'] = $name;
        }

        $section = $this->model->find($id);

        $section->update($form);
        $section->users()->sync($form['users']);

        return $section;
    }

    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }
}
