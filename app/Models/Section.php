<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        "id",
        "name",
        "description",
        "logo"
    ];

    protected $appends = [
        'logoPath'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class,'section_users');
    }

    /**
     * @return string
     */
    public function getLogoPathAttribute()
    {
        if($this->logo) {
            return 'storage/logo/'.$this->logo;
        }
        return asset('asset/images/no_image.png');
    }
}
