<?php


namespace App\Interfaces;


interface SectionInterface
{
    /**
     * @param $request
     * @return mixed
     */
    public function index($request);

    /**
     * @param $request
     * @return mixed
     */
    public function create($request);

    /**
     * @param $request
     * @return mixed
     */
    public function update($id, $request);

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function edit($id, $request = null);

    /**
     * @param $request
     * @return mixed
     */
    public function store($request);

    /**
     * @param $request
     * @return mixed
     */
    public function get($request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}
