<?php


namespace App\Interfaces;


interface UserInterface
{
    /**
     * @param $request
     * @return mixed
     */
    public function index($request);

    /**
     * @param $request
     * @return mixed
     */
    public function create($request);

    /**
     * @param $request
     * @return mixed
     */
    public function update($id, $request);

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function edit($id, $request);

    /**
     * @param $request
     * @return mixed
     */
    public function store($request);

    /**
     * @param $request
     * @return mixed
     */
    public function getList($request);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}
