<?php

namespace App\Providers;

use App\Interfaces\SectionInterface;
use App\Interfaces\UserInterface;
use App\Repositories\SectionRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            UserInterface::class,
            UserRepository::class
        );
        $this->app->bind(
            SectionInterface::class,
            SectionRepository::class
        );
    }
}
