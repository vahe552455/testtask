<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreationRequest;
use App\Http\Requests\User\EditingRequest;
use App\Interfaces\UserInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    /**
     * @var UserInterface
     */
    protected $repository;

    /**
     * UserController constructor.
     * @param UserInterface $userRepository
     */
    public function __construct(UserInterface $userRepository)
    {
        $this->repository = $userRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return $this->repository->index($request->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $list = $this->repository->getList($request->all());

        return response()->json($list,Response::HTTP_OK);
    }

    /**
     * @param CreationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreationRequest $request)
    {
        $this->repository->store($request->all());

        return redirect()->route('users.index')->with('message', 'User created successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
      $user = $this->repository->edit($id, request()->all());

       return view('users.edit', compact('user'));
    }

    /**
     * @param EditingRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditingRequest $request, $id)
    {
       $response =  $this->repository->update($id, $request->all());

        return redirect()->route('users.index')->with('message', 'User updated successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('users.index')->with('message', 'User deleted successfully');
    }
}
