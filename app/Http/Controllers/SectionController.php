<?php

namespace App\Http\Controllers;

use App\Http\Requests\Section\CreationRequest;
use App\Http\Requests\Section\EditingRequest;
use App\Interfaces\SectionInterface;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SectionController extends Controller
{
    /**
     * @var SectionInterface
     */
    protected $repository;

    /**
     * SectionController constructor.
     * @param SectionInterface $sectionRepository
     */
    public function __construct(SectionInterface $sectionRepository)
    {
        $this->repository = $sectionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sections/vue/index');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function get(Request $request)
    {
        return $this->repository->get($request->all());
    }

    /**
     * @param CreationRequest $request
     * @return mixed
     */
    public function store(CreationRequest $request)
    {
        return $this->repository->store($request->all());
    }

    /**
     * @param Section $section
     */
    public function show(Section $section)
    {
        //
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit( $id)
    {
        try {
            $section = $this->repository->edit($id);
        } catch (\Exception $exception) {
            return \response()->json($exception->getMessage());
        }

        return response()->json($section,Response::HTTP_OK);
    }

    /**
     * @param EditingRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(EditingRequest $request, $id)
    {
        $section = $this->repository->update($id, $request->all());

        return \response()->json($section, Response::HTTP_OK);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy( $id)
    {
        $deleted = $this->repository->delete($id);

        return \response()->json($deleted);
    }
}
