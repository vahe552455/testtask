<?php

namespace App\Http\Requests\Section;

use Illuminate\Foundation\Http\FormRequest;

class EditingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "form.name" => 'required',
            "image" => 'nullable|mimes:jpeg,jpg,png,gif|max:10000'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return  [
            'form.name.required' => 'Name is required',
            'image.mimes' => 'Image is not correct'
        ];

    }


    /**
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function getValidatorInstance()
    {
        $this->formatForm();

        return  parent::getValidatorInstance();
    }

    protected function formatForm()
    {
        //change data before validation

        $this->merge([
            'form' => json_decode($this->get('form'),true),
        ]);
    }
}
