(function($) {
    "use strict"; // Start of use strict

    // Toggle the side navigation
    $("#sidebarToggle").on('click', function(e) {
        e.preventDefault();
        $("body").toggleClass("sidebar-toggled");
        $(".sidebar").toggleClass("toggled");
    });

    // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
    $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
        if ($(window).width() > 768) {
            var e0 = e.originalEvent,
                delta = e0.wheelDelta || -e0.detail;
            this.scrollTop += (delta < 0 ? 1 : -1) * 30;
            e.preventDefault();
        }
    });

    // Scroll to top button appear
    $(document).on('scroll', function() {
        var scrollDistance = $(this).scrollTop();
        if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    // Smooth scrolling using jQuery easing
    $(document).on('click', 'a.scroll-to-top', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top)
        }, 1000, 'easeInOutExpo');
        event.preventDefault();
    });
    // delete any item
    //    my js
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var deletetableId;
    var href;
    $('.closs-item').on('click', function () {
        event.preventDefault();
        deletetableId = $(this).attr('data-id');
        href = $(this).attr('href');
        $('.closs-this').attr('data-get',deletetableId)
        // console.log( $('.closs-this').attr('data-get'));
    })

    $('.closs-this').on('click',function () {
        event.preventDefault();
        $(this).attr('data-get');
        console.log($(this).attr('data-get'));
        $.ajax({
            type: $(this).data('method'),
            url: href,
            data: {
                id: deletetableId
            },
            success: (data) => {
                console.log(data);
                // console.log($('#'+data).remove());
                $('#deleteModal').modal('hide');
                $('#' + data['id']).remove();
            }
        })
    });
    var user;
    var nameinput =  $("input#name");
    var namemail=  $("input#email");
    var current;
        // var hiddeninput = $("input.hidden_input");
    $('[data-target="#openModal"]').on('click',function () {
        current =$(this);
        user = jQuery.parseJSON($(this).attr('data-user'));
        $('#openModal').modal('show');
        nameinput.val(user.name);
        namemail.val(user.email);
        $("#select_country option[value=" + user.country_id + "]").prop('selected', true);
    })

//    triger click
    $('body').on('click','[data-click]',function () {
        var click = $(this).data('click');
        $('#'+click).trigger('submit');
    })

    $('#trigger_click').submit(function () {
        var url = $(this).parents('form').attr('action').replace(1,user.id)

        $.ajax({
            type:'PUT',
            url:url,
            data:{
                name: nameinput.val(),
                email: namemail.val(),
                country_id: $('#select_country').find(":selected").val(),
                country: $('#select_country').find(":selected").text()
            },
            success:function (data) {
                var datanormal = jQuery.parseJSON(data);
                console.log(datanormal)
                $('#'+datanormal.id +' .getname').text(datanormal.name );
                $('#'+datanormal.id +' .getmail').text(datanormal.email );
                $('#'+datanormal.id +' .getcountry').text(datanormal.country.name );
                current.attr('data-user',data)
            }
        })


    })

//    triger click


})(jQuery); // End of use strict
