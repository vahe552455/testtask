<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 15)->create()->each(function ($user) {
            $user->sections()->save(factory(App\Models\Section::class)->make());
        });

        \App\User::create([
        'name' => 'test',
        'email' => 'admin@test.loc',
        'email_verified_at' => now(),
        'password' => bcrypt('password'),
        'remember_token' => Str::random(10),
    ]);
    }
}
