<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function() {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('users', 'UserController')->except(['show']);
    Route::get('users/list', 'UserController@list');
    Route::get('sections', 'SectionController@index')->name('section.index');
    Route::get('sections/get', 'SectionController@get');
    Route::get('sections/{id}/edit', 'SectionController@edit');
    Route::post('sections/store', 'SectionController@store');
    Route::post('sections/{id}', 'SectionController@update');
    Route::delete('sections/{id}', 'SectionController@destroy');
});

